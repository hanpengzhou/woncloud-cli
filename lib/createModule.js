import { mkdir } from "fs/promises";
import nodegit from "nodegit";
import chalk from "chalk";
import ora from "ora";
import { cwd } from "process";
import { join } from "path";
import { init, modifyPackageJson } from "./handleFile.js";
const createTemp = function (projectName, answers, option) {
  let projectDir = join(cwd(), projectName);
  mkdir(projectDir, { recursive: true })
    .then(async (dir) => {
      const spinner = ora(`Loading ${chalk.yellow("project")}`).start();
      nodegit
        .Clone("https://gitee.com/hanpengzhou/cli-temp.git", projectName)
        .then(() => {
          //做 1根据answer选择编译工具,1创建递归文件夹.2配置菜单页面
          init(projectDir, option);
          modifyPackageJson(projectDir, answers);
          spinner.succeed(`${chalk.green("load success")}`);
          spinner.info(
            `Get started with the following commands: \n $ cd ${projectName} \n $ yarn install \n $ yarn serve`
          );
        })
        .catch((err) => {
          console.log(err);
          if (err.errno === -4) {
            spinner.fail(`${projectName} exists and is not an empty directory`);
          }
          spinner.fail(`${chalk.red("load fail")}`);
        });
    })
    .catch((err) => {
      console.log(err, "create dir fail");
    });
};

export { createTemp };
