#!/usr/bin/env node
import { Command } from "commander/esm.mjs";
import inquirer from "inquirer";
import { access, constants, readFileSync } from "fs";
import { createTemp } from "../lib/createModule.js";
import requireJson from "../lib/requireJson.cjs";

const version = process.version;
let packageJson = requireJson("../package.json");
let requiredVersion = packageJson.engines.node;
//检查node版本
function checkNodeVersion(wanted) {
  const reqVer = requiredVersion.split(">=");
  if (`v${reqVer[1]}` > version) {
    console.log(
      "You are using Node " +
        version +
        ", but this version of woncloud-cli requires Node " +
        reqVer[1] +
        ".\nPlease upgrade your Node version."
    );
    process.exit(1);
  }
}
checkNodeVersion(requiredVersion);

const program = new Command();

program
  .command("create")
  .argument("<projectName>", "PorjectName")
  .option("-c, --config <dirConfig>", "指定项目配置目录")
  .description("clone a repository into a newly created directory")
  .action(async function (projectName, options) {
    let questions = [
      {
        type: "input",
        name: "name",
        message: `please input your project name?(${projectName})`,
        choices: ["vue-cli", "vite"],
        default: projectName,
      },
      {
        type: "input",
        name: "version",
        message: `please input version`,
        default: "",
      },
      {
        type: "input",
        name: "author",
        message: `please input author`,
        default: "",
      },
    ];
    inquirer.prompt(questions).then((answers) => {
      //将answers,option,projectName传入,进行文件配置
      //判断是否有该文件?
      if (options.config) {
        access(options.config, constants.F_OK, (err) => {
          if (err) {
            console.log(`${options.config} "does not exist"`);
            process.exit(1);
          }
        });
      }
      createTemp(projectName, answers, options);
      //添加加载动效
    });
  });
program.parse();
