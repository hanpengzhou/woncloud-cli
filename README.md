<h1 align='center'>
<samp>woncloud-cli</samp> / <samp>woncloud-cli</samp>
</h1>

<p align='center'>
  <samp>瀛云脚手架</samp>
<br>
<br>
</p>

## Usage

<routeConfig>为路由菜单配置的 json 文件.格式参考 menuData.json

```bash
npx woncloud-cli create <projectName> [-c <routeConfig>]
```

#### Install globally

```bash
yarn add woncloud-cli -g
```

#### Install as dependency

```bash
yarn add woncloud-cli
```
